<?php

namespace Spiderman\Event;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class SecurityRequestListener
{
    private $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $apiKey = $event->getRequest()->headers->get('X-API-KEY');
        if (
            null == $apiKey
            || $apiKey !== $this->apiKey
        ) {
            return $event->setResponse(new JsonResponse(null, 401));
        }
        return null;
    }
}