<?php

namespace Spiderman\Controller;

use Spiderman\Entity\User;
use Spiderman\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class GetUserController
{
    private $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function action(string $externalId)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy([
            'externalId' => $externalId
        ]);

        if ($user == null) {
            return new JsonResponse(null, 404);
        }

        return new JsonResponse(
            [
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'externalId' => $user->getExternalId(),
                'username' => $user->getUsername()
            ]
        );
    }
}
