<?php

namespace Spiderman\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Spiderman\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PostUserController
{
    private $em;
    private $serializer;
    private $validator;

    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function action(Request $request)
    {
        $user = $this->serializer->deserialize($request->getContent(), User::class, 'json');
        $errors = $this->validator->validate($user);

        if (count($errors) > 0) {
            return new JsonResponse(
              ['message' => (string) $errors],
              Response::HTTP_BAD_REQUEST
            );
        }
        $this->em->persist($user);
        try {
            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            return new JsonResponse(['message' => 'duplicated unique field.'],Response::HTTP_FORBIDDEN);
        }
        return new JsonResponse([],Response::HTTP_CREATED);
    }
}
